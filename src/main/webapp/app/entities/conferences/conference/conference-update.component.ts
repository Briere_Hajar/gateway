import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IConference, Conference } from 'app/shared/model/conferences/conference.model';
import { ConferenceService } from './conference.service';

@Component({
  selector: 'jhi-conference-update',
  templateUrl: './conference-update.component.html'
})
export class ConferenceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    participants: [null, [Validators.required]],
    date: [null, [Validators.required]]
  });

  constructor(protected conferenceService: ConferenceService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ conference }) => {
      this.updateForm(conference);
    });
  }

  updateForm(conference: IConference): void {
    this.editForm.patchValue({
      id: conference.id,
      name: conference.name,
      participants: conference.participants,
      date: conference.date != null ? conference.date.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const conference = this.createFromForm();
    if (conference.id !== undefined) {
      this.subscribeToSaveResponse(this.conferenceService.update(conference));
    } else {
      this.subscribeToSaveResponse(this.conferenceService.create(conference));
    }
  }

  private createFromForm(): IConference {
    return {
      ...new Conference(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      participants: this.editForm.get(['participants'])!.value,
      date: this.editForm.get(['date'])!.value != null ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConference>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
